# Overview

Uses the Leitner system to help the user memorize items. These items and progress memorizing them are stored in a local json file.

# How to Use

1. Copy data.json to some other file, such as space_facts.json.
2. Edit the "Questions" and "Answers" lists in that file to represent what you want to memorize.
   The first question should correspond with the first answer, the second question with the second answer in the list, and so on.
   "LastShown" and "Frequency" do not need to be manually edited.
3. Run "mem -f space_facts.json"
   "LastShown" and "Frequency" are automatically updated in space_facts.json, helping the program show you questions you got right less often.
4. Optionally, set up the environment variable MEM_DUPLICATE_LOC, such as by adding this line to your .bash_profile: export MEM_DUPLICATE_LOC="$HOME/mem_duplicates" 
   If this optional variable is set, mem will write json data files to this directory in addition to the json file specified by the -f option.
   This serves as an automatic backup.

