package main

import (
    "os"
    "fmt"
    "time"
    "flag"
    "path"
    "bufio"
    "strings"
    "strconv"
    "math/rand"
    "encoding/json"
    "io/ioutil"
)

//
// Questions and information about them is stored in a .json file. Format version 0 looks like this.
// Format version 1 is the latest version and is preferred.
//
type Data0 struct {
    Questions []string
    Answers []string
    LastShown []string
    Frequency []int
}

//
// Questions and information about them is stored in a .json file. Format version 1 looks like this.
// Format version 1 is the latest version and is preferred.
//
type Data1 struct {
    Version int                 `json:"version"`   // file format version (such as 0 or 1)
    Questions []Data1Question  `json:"questions"`
}

type Data1Question struct {
    Query string      `json:"query"`
    Answer string     `json:"answer"`
    LastShown string  `json:"lastShown,omitempty"`
    Frequency int     `json:"frequency,omitempty"`
}

//
// String representation of a data object
//
func (d Data1) String() string {
    output := fmt.Sprintf("Format Version: %d\n\n", d.Version)
    for _,q := range d.Questions {
        output += fmt.Sprintf("Q: " + q.Query + "\n")
        output += fmt.Sprintf("A: " + q.Answer + "\n")
        output += fmt.Sprintf("LastShown: " + q.LastShown + "\n")
        output += fmt.Sprintf("Frequency: " + strconv.Itoa(q.Frequency) + "\n")
        output += "\n"
    }
    return output
}

//
// Read a json file, returning a data object
//
func readData(fileName string) Data1 {
    file, err := ioutil.ReadFile(fileName)
    if err != nil {
        fmt.Println("Error while reading data file")
        os.Exit(1)
    }
    var d Data1
    err = json.Unmarshal(file, &d)
    if err != nil {
        d = Data1{}     // purge improperly read values from the previous read attempt
        fmt.Println("Could not decode as data file version 1.")
        fmt.Println("Attempting to decode as data file version 0...")
        var d0 Data0
        err0 := json.Unmarshal(file, &d0)
        if err0 != nil {
            fmt.Printf("Error: Could not read data file \"%s\" as either format version 0 or 1. Message: %s\n", fileName, err)
            os.Exit(1)
        }

        // Translate data format version 0 to format version 1
        fmt.Println("Translating from version 0 to 1...")
        d.Version = 1
        for i,q := range d0.Questions {
            newQ := Data1Question{Query: q, Answer: d0.Answers[i], LastShown: d0.LastShown[i], Frequency: d0.Frequency[i]}
            d.Questions = append(d.Questions, newQ)
        }
    }
    return d
}

//
// Write a data object to a json file
//
func writeData(fileName string, d *Data1) {
    fmt.Println("Updating content of file", fileName)
    bytes, _ := json.MarshalIndent(d, "", "    ")
    err := ioutil.WriteFile(fileName, bytes, 0644)
    if err != nil {
        fmt.Println(err)
        os.Exit(1)
    }

    // write to duplicate backup file if option is set
    if os.Getenv("MEM_DUPLICATE_LOC") != "" {
        outputPath := path.Join(os.Getenv("MEM_DUPLICATE_LOC"), fileName)
        fmt.Println("Writing duplicate to", outputPath)
        err := ioutil.WriteFile(outputPath, bytes, 0644)
        if err != nil {
            fmt.Println(err)
            os.Exit(1)
        }
    }
}

//
// Turn a date such as "August 1, 2016" into a Time object
//
func stringToDate(s string) time.Time {
    t, err := time.Parse("January 2, 2006", s)
    if err != nil {
        os.Exit(1)
    }
    return t
}

//
// Turn a Time object into a string such as "August 1, 2016"
//
func dateToString(t time.Time) string {
    return t.Format("January 2, 2006")
}

//
// Return a list of indexes for the entries that are due to be quized right now
//
func findDue(d *Data1) []int {
    var indexes []int
    for i:=0; i<len(d.Questions); i++ {
        daysSince := int(time.Since(stringToDate(d.Questions[i].LastShown)).Hours()) / 24
        if daysSince >= d.Questions[i].Frequency {
            indexes = append(indexes, i)
        }
    }
    return indexes
}

//
// Take an int specifying how many days to wait between showings of a query, then return an increased value
//
func decreaseFrequency(f int) int {
    levels := []int{1,3,7,30,182,365}
    for i:=0; i<len(levels); i++ {
        if f < levels[i] {
            return levels[i]
        }
    }
    return levels[len(levels)-1]
}

//
// Return true if a file exists
//
func fileExists(path string) bool {
    if _,err := os.Stat(path); os.IsNotExist(err) {
        return false
    } else {
        return true
    }
}

//
// Randomize a given list of ints
//
func shuffleInts(src []int) []int {
    dest := make([]int, len(src))
    perm := rand.Perm(len(src))
    for i, v := range perm {
        dest[v] = src[i]
    }
    return dest
}

//
// Find oldest date a question was shown
//
func findOldest(d *Data1) string {
    result := time.Now()
    for i:=0; i<len(d.Questions); i++ {
        if stringToDate(d.Questions[i].LastShown).Before(result) {
            result = stringToDate(d.Questions[i].LastShown)
        }
    }
    return dateToString(result)
}

//
// It's valid to read in a data file that has only question and answer fields filled out,
// but sensible defaults for lastShown and frequency must be added to the data object
//
func fixBlankFields(d *Data1) {

    // Doing this doesn't make sense for the old version 0 data files
    if d.Version == 1 {
        for i,q := range d.Questions {
            if q.LastShown == "" {
                d.Questions[i].LastShown = "January 1, 1900"
            }
            if q.Frequency == 0 {
                d.Questions[i].Frequency = 1
            }
        }
    }

}

//
// Find newest date a question was shown
//
func findNewest(d *Data1) string {
    result := stringToDate("January 1, 1900")
    for i:=0; i<len(d.Questions); i++ {
        if stringToDate(d.Questions[i].LastShown).After(result) {
            result = stringToDate(d.Questions[i].LastShown)
        }
    }
    return dateToString(result)
}

//
// Main program execution begins here
//
func main() {

    // seed the random number generator
    rand.Seed(time.Now().UTC().UnixNano())

    // parse command-line flags
    var fParam string
    flag.StringVar(&fParam, "f", "", "The location of the data file to use")
    var rParam bool
    flag.BoolVar(&rParam, "R", false, "If present, prevents the order of questions from being randomized when asked")
    var sParam bool
    flag.BoolVar(&sParam, "s", false, "If present, shows status information about the data file, then exits")
    var vParam bool
    flag.BoolVar(&vParam, "v", false, "If present, shows the version number of the program, then exits")
    flag.Parse()

    // show the version number
    if vParam {
        fmt.Println("v2")
        os.Exit(0)
    }

    // use default data file location or a location specified by the user
    dataFile := ""
    if fParam != "" {
        dataFile = fParam
        if fileExists(dataFile) == false {
            fmt.Printf("Error: Data file \"%s\" not found.\n", dataFile)
            os.Exit(1)
        }
    } else if fileExists(dataFile) == false {
        fmt.Println("Error: No data file found. The -f option is required.")
        os.Exit(1)
    }
    fmt.Printf("Using data file: %s\n\n", dataFile)
    d := readData(dataFile)

    fixBlankFields(&d)

    // show status info, then exit
    if sParam == true {
        fmt.Printf("%-33s: %s\n", "File", dataFile)
        fmt.Printf("%-33s: %d\n", "Number of questions", len(d.Questions))
        fmt.Printf("%-33s: %d\n", "Number of questions due", len(findDue(&d)))
        fmt.Printf("%-33s: %s\n", "Longest ago a question was shown", findOldest(&d))
        fmt.Printf("%-33s: %s\n", "Most recent question show date", findNewest(&d))
        os.Exit(0)

    // have the user answer questions
    } else {

        // make a list of indexes that point to the questions that are due to be answered
        dueIndexes := findDue(&d)
        if rParam == false {
            dueIndexes = shuffleInts(dueIndexes)
        }

        reader := bufio.NewReader(os.Stdin)
        for i:=0; i<len(dueIndexes); i++ {
            curIndex := dueIndexes[i]
            fmt.Println(d.Questions[curIndex].Query)
            fmt.Printf("[%d/%d] press enter to continue (e to exit)> ", i+1, len(dueIndexes))
            continueKey, _ := reader.ReadString('\n')
            continueKey = strings.TrimSpace(continueKey)
            if continueKey == "e" {
                fmt.Println("Exit")
                break
            }
            fmt.Printf("Answer: %s\n", d.Questions[curIndex].Answer)
            fmt.Print("Was this correct? (Y/n, e to exit) > ")
            corrYN, _ := reader.ReadString('\n')
            corrYN = strings.TrimSpace(corrYN)
            if strings.ToLower(corrYN) == "y" {
                fmt.Println("=== Correct! ===")
                d.Questions[curIndex].LastShown = dateToString(time.Now())
                d.Questions[curIndex].Frequency = decreaseFrequency(d.Questions[curIndex].Frequency)
            } else if corrYN == "e" {
                fmt.Println("Exit")
                break
            } else {
                fmt.Println("=== Not correct === ")
                d.Questions[curIndex].LastShown = dateToString(time.Now())
                d.Questions[curIndex].Frequency = 1
            }
            fmt.Println()
        }
    }

    writeData(dataFile, &d)
}

